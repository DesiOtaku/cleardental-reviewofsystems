import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Button {
    id: saveButton
    property real iconSizer: height
    property var oldColor
    property var nameItems
    text: "Save and Commit"
    icon.name: "document-save"
    icon.height: iconSizer
    icon.width: iconSizer

    background: Rectangle {
        opacity: enabled ? 1 : 0.3
        border.color: saveButton.down ?"#82faff":"#86c1ff"
        color: saveButton.down ?"#82faff":"#86c1ff" ;
        border.width: 1

    }
    onClicked:  {
        //first the regular conditions
        var jsonData = [];
        for(var conditionPageIndex=0;conditionPageIndex< conditionPages.length;
            conditionPageIndex++) {
            var page = conditionPages[conditionPageIndex];
            var conditons = page.conditionInfo;
            for(var conditionName in conditons) {
                var addMe = {"ConditionName": conditionName,
                    "ConditionInfo": conditons[conditionName]};
                jsonData.push(addMe);
            }
        }

        //Then other conditions
        var others = otherPage.makeConditionList();
        if(others !== false) {
            for(var otherIndex=0;otherIndex< others.length;otherIndex++) {
                var addMeOther = {"ConditionName": others[otherIndex],
                    "ConditionInfo": ""};
            }
        }

        //Then PregnancyPage


        oldColor = saveButton.icon.color
        confirmGood.start();
    }

    SequentialAnimation {
        id: confirmGood
        PropertyAnimation  {
            target: saveButton
            property: "iconSizer"
            from: saveButton.height
            to: 0
            duration: 200
        }
        ScriptAction {
            script: {
                saveButton.icon.name = "dialog-ok"
                saveButton.icon.color= "green"
            }
        }
        PropertyAnimation {
            target: saveButton
            property: "iconSizer"
            to: saveButton.height
            from: 0
            duration: 200
        }


        PauseAnimation {
            duration: 1000
        }

        PropertyAnimation  {
            target: saveButton
            property: "iconSizer"
            from: saveButton.height
            to: 0
            duration: 200
        }
        ScriptAction {
            script: {
                saveButton.icon.name = "document-save";
                saveButton.icon.color= oldColor;
            }
        }
        PropertyAnimation {
            target: saveButton
            property: "iconSizer"
            to: saveButton.height
            from: 0
            duration: 200
        }


    }
}
