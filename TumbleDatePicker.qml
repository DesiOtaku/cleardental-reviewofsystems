import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10

Row {

    function getDate() {
        var month = monthBox.currentIndex+ 1;
        var day = dayBox.currentText;
        var year = yearBox.currentText;
        return month + "/" + day + "/" + year; //US standard, I know...
    }

    function setDate(newDate) {
        if(newDate.length > 3)  {
            var partsOfDay = newDate.split("/");
            monthBox.currentIndex = partsOfDay[0] -1;
            dayBox.currentIndex = dayBox.find(partsOfDay[1]);
            yearBox.currentIndex = yearBox.find(partsOfDay[2]);
        }
    }

    ComboBox {
        id: monthBox
        width: 120
        model: ['January', 'February', 'March', 'April',
            'May', 'June', 'July', 'August', 'September',
            'October', 'November', 'December'];
    }

    ComboBox {
        id: dayBox
        width: 75
        model:  {
            var returnMe= [];
            for(var i=1;i<33;i++) {
                returnMe.push(i);
            }
            return returnMe;
        }
    }

    ComboBox {
        id: yearBox
        width: 85
        model:  {
            var returnMe= [];
            for(var i=2019;i<2030;i++) {
                returnMe.push(i);
            }
            return returnMe;
        }
        currentIndex: 100
    }


}
