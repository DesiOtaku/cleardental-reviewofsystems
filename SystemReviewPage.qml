import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.11

Page {
    property var systemName
    property var conditionList
    property var conditionInfo: ({});

    signal weAreDoneHere

    title: systemName

    Text {
        id: name
        text: "Are there any <b>"  + systemName + "</b> you know of?";
        font.pointSize: 16
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 10
    }

    Text {
        id: exampleTexts
        anchors.top: name.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: name.height
        width: parent.width * .90
        elide: Text.ElideRight
        horizontalAlignment: Text.AlignHCenter

        text:  {
            var returnMe="This includes: ";
            for(var cond in conditionList) {
                returnMe += conditionList[cond] + ", ";
            }
            returnMe = returnMe.substring(0, returnMe.length - 2);
            returnMe += ".";

            return returnMe;
        }

        Behavior on opacity {
            NumberAnimation {
                duration: 500
            }
        }
    }

    RowLayout {
        id: yesNoRow
        anchors.top: exampleTexts.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: name.height
        spacing: yesButton.height
        Button {
            id: yesButton
            text: "Yes"
            onClicked:{
                yesNoRow.opacity=0;
                exampleTexts.opacity=0;
                conditionListView.visible = true;
                conditionModel.makeConditionModel();
            }

            background: Rectangle {
                opacity: enabled ? 1 : 0.3
                implicitWidth: 100
                implicitHeight: 40
                border.color: yesButton.down ?"#82faff":"#86c1ff"
                color: yesButton.down ?"#82faff":"#86c1ff" ;
                border.width: 1
            }

        }
        Button {
            id: noButton
            text: "No"
            onClicked: weAreDoneHere();

            background: Rectangle {
                opacity: enabled ? 1 : 0.3
                implicitWidth: 100
                implicitHeight: 40
                border.color: noButton.down ?"#fffa82":"#ffc186"
                color: noButton.down ?"#ff6666":"#ffaaaa" ;
                border.width: 1
            }
        }

        Behavior on opacity {
            NumberAnimation {
                duration: 500
            }
        }
    }

    Timer {
        id: addTimer
        interval: 100
        repeat: true

        onTriggered: {
            if(!conditionModel.done) {
                conditionModel.addNextItem();
            } else {
                repeat = false;
            }
        }
    }

    ListModel {
        id: conditionModel

        property bool done: false
        property int currIndex: 0

        function makeConditionModel() {
            done = false;
            currIndex =0;
            addNextItem();
            addTimer.start();
        }

        function addNextItem() {
            conditionModel.append({conditionName:conditionList[currIndex]});
            currIndex++;
            if(currIndex >= conditionList.length) {
                done = true;
            }
        }
    }

    Component {
        id: contitionComp
        Rectangle {
            id: rectComp
            height: conText.checked ?
                        conText.height + explainMoreField.height+ 10 :
                        conText.height + 5
            radius: 5
            width: conditionListView.width
            border.width: 1
            border.color: "black"
            color: "#CCCCEE"
            ColumnLayout {
                CheckBox {
                    id: conText
                    Layout.leftMargin: 5
                    text: conditionName
                    font.pointSize: 12
                    width: parent.width
                    onCheckStateChanged: {
                        if(checked) {
                            conditionInfo[conditionName] = explainMoreField.text;
                        }
                        else {
                            delete conditionInfo[conditionName];
                        }
                    }
                }
                TextField {
                    id: explainMoreField
                    Layout.leftMargin: 10
                    opacity: conText.checked ? 1 : 0
                    implicitWidth: rectComp.width - 20
                    placeholderText: "Please explain more about the " + conditionName
                    onTextChanged: conditionInfo[conditionName] = explainMoreField.text;
                    Behavior on opacity {
                        NumberAnimation {
                            duration: 300
                        }
                    }
                }
            }

            Behavior on height {
                NumberAnimation {
                    duration: 250
                }
            }
        }
    }

    ListView {
        id: conditionListView
        anchors.top: name.bottom
        anchors.topMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 5
        anchors.right: parent.right
        anchors.rightMargin: 5
        anchors.bottom: parent.bottom
        model: conditionModel
        clip: true
        delegate: contitionComp
        spacing: 5
        ScrollBar.vertical: ScrollBar { }
        visible: false

        add: Transition {
            NumberAnimation { property: "scale"; from: 0; to: 1.0; duration: 500 }
            NumberAnimation { properties: "opacity"; from: 0; to: 1; duration: 500 }
        }
    }

    footer: Button {
        id: doneWithConButton
        text: "Done with condition"
        opacity: conditionListView.visible ? 1:0
        Behavior on opacity {
            NumberAnimation {
                duration: 500
            }
        }

        onClicked: weAreDoneHere();
    }

}
