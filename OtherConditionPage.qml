import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.11

Page {
    property var systemName
    signal weAreDoneHere

    title: "Other conditions"

    Text {
        id: name
        text: "Are there any other conditions not previously listed?";
        font.pointSize: 16
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 10
    }

    RowLayout {
        id: yesNoRow
        anchors.top: name.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: name.height
        spacing: yesButton.height
        Button {
            id: yesButton
            text: "Yes"
            onClicked:{
                yesNoRow.opacity=0;
                moreCondColumn.visible = true;
                moreCondColumn.opacity=1;
            }

            background: Rectangle {
                opacity: enabled ? 1 : 0.3
                implicitWidth: 100
                implicitHeight: 40
                border.color: yesButton.down ?"#82faff":"#86c1ff"
                color: yesButton.down ?"#82faff":"#86c1ff" ;
                border.width: 1
            }

        }
        Button {
            id: noButton
            text: "No"
            onClicked: weAreDoneHere();

            background: Rectangle {
                opacity: enabled ? 1 : 0.3
                implicitWidth: 100
                implicitHeight: 40
                border.color: noButton.down ?"#fffa82":"#ffc186"
                color: noButton.down ?"#ff6666":"#ffaaaa" ;
                border.width: 1
            }
        }

        Behavior on opacity {
            NumberAnimation {
                duration: 500
            }
        }
    }

    Column {
        id: moreCondColumn
        visible: false
        opacity: 0
        anchors.top: name.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        Label {
            id: pleaseMentionText
            text: "Please mention all of them here:"
            font.pointSize: 12
        }

        Rectangle {
            width: pleaseMentionText.width *2
            height: width
            border.color: "black"
            border.width: 2

            TextArea {
                id: tellMeMore
                anchors.fill: parent
                wrapMode: TextEdit.WordWrap

            }
        }

        Behavior on opacity {
            NumberAnimation {
                duration: 500
            }
        }
    }


    footer: Button {
        id: doneWithConButton
        text: "Done with condition"
        opacity: moreCondColumn.visible ? 1:0
        Behavior on opacity {
            NumberAnimation {
                duration: 500
            }
        }

        onClicked: weAreDoneHere();
    }

    function makeConditionList() {
        if(moreCondColumn.visible) {
            return tellMeMore.text.split("\n");
        } else {
            return false;
        }
    }

}
