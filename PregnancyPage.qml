import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.11

Page {
    property var systemName
    property var conditionList

    signal weAreDoneHere

    title: "Pregnant"

    Text {
        id: name
        text: "Are you currently or trying to become pregnant?";
        font.pointSize: 16
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 10
    }

    RowLayout {
        id: yesNoRow
        anchors.top: name.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: name.height
        spacing: yesButton.height
        Button {
            id: yesButton
            text: "Yes"
            onClicked:{
                yesNoRow.opacity=0;
                moreInfoGrid.visible = true;
                moreInfoGrid.opacity = 1;
            }

            background: Rectangle {
                opacity: enabled ? 1 : 0.3
                implicitWidth: 100
                implicitHeight: 40
                border.color: yesButton.down ?"#82faff":"#86c1ff"
                color: yesButton.down ?"#82faff":"#86c1ff" ;
                border.width: 1
            }

        }
        Button {
            id: noButton
            text: "No"
            onClicked: weAreDoneHere();

            background: Rectangle {
                opacity: enabled ? 1 : 0.3
                implicitWidth: 100
                implicitHeight: 40
                border.color: noButton.down ?"#fffa82":"#ffc186"
                color: noButton.down ?"#ff6666":"#ffaaaa" ;
                border.width: 1
            }
        }

        Behavior on opacity {
            NumberAnimation {
                duration: 500
            }
        }
    }

    Grid {
        id: moreInfoGrid
        columns: 2
        anchors.top: name.bottom
        anchors.topMargin: 10
        spacing: 10
        anchors.horizontalCenter: parent.horizontalCenter
        opacity: 0
        visible: false

        Label {
            text: "What is the due date?"
            font.pointSize: 16
            verticalAlignment: Text.AlignVCenter
            height: dueDate.height
        }

        TumbleDatePicker {
            id: dueDate
            Component.onCompleted: {
                setDate((new Date()).toLocaleString(Qt.locale(),"MM/d/yyyy"))
            }
        }

        Label {
            text: "What is the name of the OB/GYN?"
            font.pointSize: 16
            verticalAlignment: Text.AlignVCenter
            height: obgynName.height
        }

        TextField {
            id: obgynName
            placeholderText: "Name of the OB/GYN"
        }

        Label {
            text: "Are you currently breast feeding?"
            font.pointSize: 16
            verticalAlignment: Text.AlignVCenter
            height: obgynName.height
        }

        Row {
            RadioButton {
                text: "Yes"
            }
            RadioButton {
                text: "No"
            }
        }

        Behavior on opacity {
            NumberAnimation {
                duration: 500
            }
        }
    }

    footer: Button {
        id: doneWithConButton
        text: "Done with this topic"
        opacity: moreInfoGrid.opacity==1 ? 1:0
        Behavior on opacity {
            NumberAnimation {
                duration: 500
            }
        }

        onClicked: weAreDoneHere();
    }

}
