import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.11

ApplicationWindow {
    id: rootWin
    visible: true
    width: 1024
    height: 768
    title: qsTr("Review of Systems")
    property var conditionPages: [heartPage,metabolicPage,
    gastrointestinalPage,bonePage,respiratoryPage,organPage,
    infectionPage,neurologicalPage,immunePage,behaviorPage]
    property alias theOtherPage: otherPage

    SwipeView {
        id: mainSwipeView
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: indicator.top

        SystemReviewPage {
            id: heartPage
            systemName: "Heart/Blood disorders";
            conditionList: ["Artificial Heart Valve(s)",
                "Congenital Heart Defects",
                "Heart Murmurs",
                "Angina",
                "Congestive Heart Failure",
                "Heart Surgery",
                "Pacemaker / defibrillator",
                "Bacterial Endocarditis",
                "Coronary Artery Disease",
                "High Blood Pressure",
                "Low Blood Pressure",
                "Abnormal Bleeding",
                "Methemoglobinemia",
                "Hemophillia",
                "Anemia",
                "Other Heart Conditions"
            ]
            onWeAreDoneHere: mainSwipeView.currentIndex++
        }
        SystemReviewPage {
            id: metabolicPage
            systemName: "Metabolic / Endocrine disorders";
            conditionList: ["Diabetes",
                "Hypothyroidism",
                "Hyperthyroidism",
                "Other metabolic or endocrine disorder"
            ];
            onWeAreDoneHere: mainSwipeView.currentIndex++
        }
        SystemReviewPage {
            id: gastrointestinalPage
            systemName: "Gastrointestinal disorders";
            conditionList: ["G.E. Re-flux / Heartburn",
                "Gastric Ulcers / Gastritis",
                "Inflammatory Bowell Disease",
                "Other Gastrointestinal disorders"
            ];
            onWeAreDoneHere: mainSwipeView.currentIndex++
        }
        SystemReviewPage {
            id: bonePage
            systemName: "Bone / Joint disorders";
            conditionList: ["Artificial joint",
                "Osteoporosis",
                "History of Bisphosphonates (including Fosamax, Actonel, Boniva)",
                "Other Bone / Joint disorders"
            ];
            onWeAreDoneHere: mainSwipeView.currentIndex++
        }
        SystemReviewPage {
            id: respiratoryPage
            systemName: "Respiratory / Lung disorders";
            conditionList: ["Asthma",
                "Emphysema or COPD",
                "Bronchitis",
                "Other Respiratory / Lung condition"
            ];
            onWeAreDoneHere: mainSwipeView.currentIndex++
        }
        SystemReviewPage {
            id: organPage
            systemName: "Organ disorders";
            conditionList: ["Kidney Disease",
                "Liver disease",
                "Eye disease (including glaucoma)",
                "Other organ disorder"];
            onWeAreDoneHere: mainSwipeView.currentIndex++
        }
        SystemReviewPage {
            id: infectionPage
            systemName: "Infectious disease";
            conditionList: ["AIDS / HIV",
                "Hepatitis",
                "Other Sexually Transmitted Disease",
                "Tuberculosis",
                "Other infectious disease"
            ];
            onWeAreDoneHere: mainSwipeView.currentIndex++
        }
        SystemReviewPage {
            id: neurologicalPage
            systemName: "Neurological disorders";
            conditionList: ["Epilepsy",
                "Stroke",
                "Migrine",
                "Other Neurological disorders"
            ];
            onWeAreDoneHere: mainSwipeView.currentIndex++
        }
        SystemReviewPage {
            id: immunePage
            systemName: "Immune diseases";
            conditionList: ["Lupus",
                "Rheumatoid Arthritis",
                "Sjögren’s syndrome",
                "Myasthenia Gravis",
                "Other immune diseases"
            ];
            onWeAreDoneHere: mainSwipeView.currentIndex++
        }
        SystemReviewPage {
            id: behaviorPage
            systemName: "Psychotic / Behavioral disorders";
            conditionList: ["ADD / ADHD",
                "Anxiety / Panic Attacks",
                "Eating disorder",
                "Other behavioral disorders"
            ];
            onWeAreDoneHere: mainSwipeView.currentIndex++
        }

        PregnancyPage {
            id: pregPage
            onWeAreDoneHere: mainSwipeView.currentIndex++
        }

        OtherConditionPage {
            id: otherPage
            onWeAreDoneHere: mainSwipeView.currentIndex = 0;
        }
    }

    PageIndicator {
        id: indicator

        count: mainSwipeView.count
        currentIndex: mainSwipeView.currentIndex

        anchors.bottom: footLayout.top
        anchors.horizontalCenter: footLayout.horizontalCenter
    }

    footer: Row {
        id: footLayout
        SaveButton {
            width: parent.width/2
            height: doneButton.height
        }

        DelayButton {
            id: doneButton
            text: "Hold to quit"
            delay: 1000
            onActivated: Qt.quit();
            width: parent.width /2
        }
    }

    InputPanel {
        id: inputPanel
        z: 99
        x: 0
        y: rootWin.height
        width: rootWin.width
        active: false

        states: State {
            name: "visible"
            when: inputPanel.active
            PropertyChanges {
                target: inputPanel
                y: rootWin.height - inputPanel.height
            }
        }
        transitions: Transition {
            from: ""
            to: "visible"
            reversible: true
            ParallelAnimation {
                NumberAnimation {
                    properties: "y"
                    duration: 250
                    easing.type: Easing.InOutQuad
                }
            }
        }
    }
}
